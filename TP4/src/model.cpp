#include <stdio.h>
#include <string>
#include "image_ppm.h"

int main(int argc, char** argv) {
    char cNomImgLue[250], cNomImgAvg[250];
    int nH, nW, nTaille;
    
    if (argc != 3) {
        printf("Usage: ImageIn.pgm imageAverage.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue);
    sscanf (argv[2],"%s",cNomImgAvg);

    OCTET *ImgIn, *ImgAvg;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    allocation_tableau(ImgAvg, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    lire_image_pgm(cNomImgAvg, ImgAvg, nH * nW);

    float PSNR = psnr(ImgAvg, ImgIn, nTaille);

    printf("%f\n", PSNR);

    free(ImgIn);
    free(ImgAvg);
    return 1;
}