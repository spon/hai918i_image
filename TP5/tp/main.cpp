#include <igl/opengl/glfw/Viewer.h>

Eigen::Vector3d getBarycenter(Eigen::MatrixXd points)
{
  Eigen::Vector3d result(0, 0, 0);
  
  for(int i = 0; i < points.rows(); i ++)
  {
    for(int j = 0; j < points.cols(); j ++)
    {
      result(j) += points(i,j);

    }

  }

  result /= points.rows();  
  return result;
}

Eigen::Vector3d cartesianToSpherical(Eigen::Vector3d cartesian, Eigen::Vector3d  barycenter)
{
  Eigen::Vector3d position = cartesian - barycenter;
  Eigen::Vector3d result(0, 0, 0);
  for(int i = 0; i < 3; i ++)
    result(0) += position(i) * position(i);
  
  result(0) = sqrt(result(0));
  result(1) = atan2(position(0), position(1));
  result(2) = acos(position(2) / result(0));

  return result;
}

Eigen::Vector3d sphericalToCartesian(Eigen::Vector3d spherical, Eigen::Vector3d  barycenter)
{
  Eigen::Vector3d result(0, 0, 0);
  result(0) = spherical(0) * cos(spherical(1)) * sin(spherical(2)) + barycenter(0);
  result(1) = spherical(0) * sin(spherical(1)) * sin(spherical(2)) + barycenter(1);
  result(2) = spherical(0) * cos(spherical(2)) + barycenter(2);
  return result;
}

// Input is a list of all p_i, output is a matrix (N, (Bmin, Bmax))
Eigen::MatrixXd getMinMaxBin(Eigen::VectorXd p, int N)
{
  Eigen::MatrixXd result(N, 2);
  double Bmin, Bmax, Pmax, Pmin;
  Pmax = 0;
  Pmin = 100;

  for(int i = 0; i < p.rows(); i ++)
  {
    Pmax = p(i) > Pmax ? p(i) : Pmax;
    Pmin = p(i) < Pmin ? p(i) : Pmin;
  }


  for(int i = 0; i < N; i ++)
  {
    Bmin = Pmin + ((Pmax - Pmin) / N) * i;
    Bmax = Pmin + ((Pmax - Pmin) / N) * (i + 1);

    result(i,0) = Bmin;
    result(i,1) = Bmax;

  }

  return result;
}


double getNormalized(double pi, double min, double max)
{
  double result = (pi - min) / (max - min);
  return result;
}

double getDenormalized(double value, double min, double max)
{
  return value * (max  - min) + min;

}


//returns Bins. Bin(i, j) = index of j-th vertice in Bin i.
std::vector<std::vector<int>> sortInBins(std::vector<Eigen::Vector3d> spherical, int K)
{
  int vertSize = spherical.size();
  Eigen::VectorXd p(vertSize);
  for(int i = 0; i < vertSize; i ++)
    p(i) = spherical[i](0);

  Eigen::MatrixXd MinMaxBin = getMinMaxBin(p, K);
  std::vector<std::vector<int>> Bin;
  Bin.resize(K);


  double min, max;
  for(int i = 0; i < vertSize; i ++)
  {
    
    for(int j = 0; j < K; j ++)
    {
      min = MinMaxBin(j, 0);
      max = MinMaxBin(j, 1);

      // std::cout<<"min = " <<min<<" max = "<<max<<std::endl;
      if(p(i) >= min && p(i) <= max)
      {
        Bin[j].resize(Bin[j].size() + 1);
        Bin[j][Bin[j].size() - 1] = i;                
      }

    }
  }

  return Bin;

}

// Alpha has to be 0 < alpha < 1/2
Eigen::MatrixXd tattoo(Eigen::MatrixXd Vertices, char msg, int K, float alpha, float force)
{
  Eigen::Vector3d vertex;
  Eigen::Vector3d cartesianTest;
  Eigen::Vector3d barycenter;
  Eigen::Vector3d vertexSpherical;
  std::vector<Eigen::Vector3d> spherical;
  Eigen::MatrixXd outVertices(Vertices.rows(), 3);
  Eigen::VectorXd norms(Vertices.rows());
  
  //0. Get barycenter of mesh
  barycenter = getBarycenter(Vertices);

  //1. Convert cartesian coordinates to spherical ones
  spherical.resize(Vertices.rows());
  for(unsigned int i = 0; i < Vertices.rows(); i ++)
  {
    vertex(0) = Vertices(i, 0);
    vertex(1) = Vertices(i, 1);
    vertex(2) = Vertices(i, 2);


    spherical[i] = cartesianToSpherical(vertex, barycenter);
    norms[i] = spherical[i](0);
    
  }

  //2. Sort norms (spherical(0)) in K bins
  Eigen::MatrixXd minMaxBin = getMinMaxBin(norms, K);
  std::vector<std::vector<int>> Bin = sortInBins(spherical, K);

  //3. Map each Bin in normalized in range [0,1]
  double min, max;
  for(unsigned int i = 0; i < K; i ++)
  {
    min = minMaxBin(i, 0);
    max = minMaxBin(i, 1);
    
    for(unsigned int j = 0; j < Bin[i].size(); j ++)
    {
      norms(Bin[i][j]) = getNormalized(norms(Bin[i][j]), min, max);
    }
  }

  //4. Mean of each bin
  std::vector<double> binMean;
  binMean.resize(K);

  for(unsigned int i = 0; i < K; i ++)
  {
    for(unsigned int j = 0; j < Bin[i].size(); j ++)
    {
      binMean[i] += norms(Bin[i][j]);
    }

    if(!Bin[i].empty())
    {
      binMean[i] /= Bin[i].size();
    }
  }

  //5. Iterative mean shifting depending on watermark (+1 / -1)
  float kn;
  int w, Mn;
  float transformedMean;
  for(int i = 0; i < K; i ++)
  {
    Mn = Bin[i].size();
    w = msg&(1<<i);
    kn = 1;
    if(w > 0)
    {
      //watermark: +1
      do
      {
        transformedMean = 0;
        for(int j = 0; j < Mn; j ++)
        {
          norms[Bin[i][j]] = pow(norms[Bin[i][j]], kn);
          transformedMean += norms[Bin[i][j]];
        }

        transformedMean /= float(Mn);
        kn = kn - force;

      } while (transformedMean < (1.f/2.f + alpha));
      
      
    }
    else
    {
      //watermark: -1
      do
      {
        transformedMean = 0;
        for(int j = 0; j < Mn; j ++)
        {
          norms[Bin[i][j]] = pow(norms[Bin[i][j]], kn);
          transformedMean += norms[Bin[i][j]];
        }

        transformedMean /= float(Mn);
        kn = kn + force;
      } while (transformedMean > (1.f/2.f - alpha));

    }
  }

  //6. Denormalize vertex norms
  float outNorm;
  int vertIndex;
  Eigen::Vector3d outSpherical, outCartesian;
  for(int i = 0; i < K; i ++)
  {
    Mn = Bin[i].size();
    min = minMaxBin(i, 0);
    max = minMaxBin(i, 1);

    for(int j = 0; j < Mn; j ++)
    {
      vertIndex = Bin[i][j];
      outNorm = getDenormalized(norms[vertIndex], min, max);
      outSpherical(0) = outNorm;
      outSpherical(1) = spherical[vertIndex](1);  //theta
      outSpherical(2) = spherical[vertIndex](2);  //phi  

      outCartesian = sphericalToCartesian(outSpherical, barycenter);

      outVertices(vertIndex, 0) = outCartesian(0);
      outVertices(vertIndex, 1) = outCartesian(1);
      outVertices(vertIndex, 2) = outCartesian(2);
    }
  }

  return outVertices;
}

void extract(Eigen::MatrixXd Vertices, char &msg, int K)
{
  msg = 0;
  Eigen::Vector3d vertex;
  Eigen::Vector3d barycenter;
  Eigen::Vector3d vertexSpherical;
  std::vector<Eigen::Vector3d> spherical;
  Eigen::MatrixXd outVertices(Vertices.rows(), 3);
  Eigen::VectorXd norms(Vertices.rows());
  
  //0. Get barycenter of mesh
  barycenter = getBarycenter(Vertices);

  //1. Convert cartesian coordinates to spherical ones
  spherical.resize(Vertices.rows());
  for(unsigned int i = 0; i < Vertices.rows(); i ++)
  {
    vertex(0) = Vertices(i, 0);
    vertex(1) = Vertices(i, 1);
    vertex(2) = Vertices(i, 2);

    spherical[i] = cartesianToSpherical(vertex, barycenter);
    norms[i] = spherical[i](0);
    
  }


  //2. Sort norms (spherical(0)) in K bins
  Eigen::MatrixXd minMaxBin = getMinMaxBin(norms, K);
  std::vector<std::vector<int>> Bin = sortInBins(spherical, K);

  //3. Map each Bin in normalized in range [0,1]
  double min, max;
  for(unsigned int i = 0; i < K; i ++)
  {
    min = minMaxBin(i, 0);
    max = minMaxBin(i, 1);
    
    for(unsigned int j = 0; j < Bin[i].size(); j ++)
    {
      norms(Bin[i][j]) = getNormalized(norms(Bin[i][j]), min, max);
    }
  }

  //4. Mean of each bin
  std::vector<double> binMean;
  binMean.resize(K);
  float mean;
  int bit;
  for(unsigned int i = 0; i < K; i ++)
  {
    for(unsigned int j = 0; j < Bin[i].size(); j ++)
    {
      binMean[i] += norms(Bin[i][j]);
    }

    if(!Bin[i].empty())
    {
      binMean[i] /= Bin[i].size();
      mean = binMean[i];
      if(mean > 1.f/2.f)
        bit = 1;

      msg += bit * pow(2, i);
      bit = 0;
    }
  }

}

Eigen::MatrixXd V;
Eigen::MatrixXi F;

int main(int argc, char *argv[])
{

  igl::readOFF("bunny.off", V, F);

  // Plot the mesh
  igl::opengl::glfw::Viewer viewer;
  // viewer.data().set_mesh(V, F);
  
  Eigen::Vector3d barycenter = getBarycenter(V);
  Eigen::Vector3d spherical = cartesianToSpherical(Eigen::Vector3d(V(0, 0), V(0, 1), V(0, 2)), barycenter);
  sphericalToCartesian(spherical, barycenter);

  char msg = 'a';
  std::cout<<"Char hidden: "<<msg<<std::endl;
  char decrypted;
  Eigen::MatrixXd vert = tattoo(V, msg, 8, 0.3, 0.024);
  viewer.data().set_mesh(vert, F);
  extract(vert, decrypted, 8);
  std::cout<<"Decrypted msg: "<<decrypted<<std::endl;
  viewer.data().set_face_based(true);
  viewer.launch();

}
