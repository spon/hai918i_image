#include <stdio.h>
#include "image_ppm.h"
#include "../include/AES.h"

void encryptAES(OCTET* in, OCTET* out, int size, OCTET* key, OCTET* iv) {
    AES aes(AESKeyLength::AES_128);
    OCTET* crypted = aes.EncryptECB(in, size * sizeof(OCTET), key);

    for(int i = 0; i < size; i ++) {
        out[i] = crypted[i];
    }
}

void decryptAES(OCTET* in, OCTET* out, int size, OCTET* key, OCTET* iv) {
    AES aes(AESKeyLength::AES_128);
    OCTET* crypted = aes.DecryptECB(in, size * sizeof(OCTET), key);

    for(int i = 0; i < size; i ++) {
        out[i] = crypted[i];
    }
}

int main(int argc, char** argv) {
    char cNomImgLue[250], cNomImgEcrite[250];
    std::string usage;
    int nH, nW, nTaille;

    if (argc != 4) 
    {
        printf("Usage: ImageIn.pgm imageOut.pgm encrypt/decrypt\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue);
    sscanf (argv[2],"%s",cNomImgEcrite);
    usage = argv[3];

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    allocation_tableau(ImgOut, OCTET, nTaille);
    OCTET key[] = "azeazeazeazeaze";
    OCTET iv[] = "012345678901234";

    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    if(usage == "encrypt") {

        encryptAES(ImgIn, ImgOut, nTaille, key, iv);

    }else if(usage == "decrypt") {

        decryptAES(ImgIn, ImgOut, nTaille, key, iv);

    }else {

        printf("USAGE REQUIRED: encrypt or decrypt.\n");

    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn);
    free(ImgOut);
    return 1;
}