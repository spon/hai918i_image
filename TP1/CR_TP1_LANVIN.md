# TP1 Chiffrement d'images
|     Image de base utilisée tout au long      |
| :------------------------------------------: |
| <img src="capy_pgm.png" style="zoom:50%;" /> |



## Chiffrement par mélange
|              Image non modifiée              |                Image chiffrée                 |
| :------------------------------------------: | :-------------------------------------------: |
| <img src="capy_pgm.png" style="zoom:50%;" /> | <img src="scrambled.png" style="zoom:50%;" /> |

```c++
void scramble(OCTET* in, OCTET* out, int size, int* map, int key) {
    int pos;
    // rand seed is key
    srand(key);
    
    for(int i = 0; i < size; i ++) {
        pos = rand() % size;

        if(map[pos] != -1)
            pos = nextAvailable(map, size, pos);

        out[pos] = in[i];
        map[pos] = 1;
    }
}
```

La fonction ``nextAvailable(map, size, pos)`` renvoie l'indice du prochain pixel disponible de l'image. Afin de décrypter l'image chiffrée auparavant, il faut avoir la clée pour générer les mêmes valeurs que lors du chiffrement puis remplacer ``out[pos] = in[i];`` par ``out[i] = in[pos];``.
$$
psnr = 10.60
$$

$$
entropy = 7.35
$$

Pour l'image *chiffrée par mélange*, on remarque que l'histogramme est alors identique à l'image de base puisque les pixels sont les mêmes ils sont simplements mélangés par ra pport à l'image de base.

|         Histogramme image de base         |        Histogramme image chiffrée         |
| :---------------------------------------: | :---------------------------------------: |
| <img src="histo.png" style="zoom:50%;" /> | <img src="histo.png" style="zoom:50%;" /> |



## Chiffrement par substitution
| Image non modifiée |   Image chiffrée    |
| :----------------: | :-----------------: |
| ![](capy_pgm.png)  | ![](substitute.png) |

```c++
void substitute(OCTET* in, OCTET* out, int size, int key) {
    srand(key);
    int k = rand() % 256;
    out[0] = k + in[0];

    for(int i = 1; i < size; i ++) {
        k = rand() % 256;
        out[i] = (out[i - 1] + in[i] + k) % 256;
    }
}
```

Pour décrypter une image chiffrée à l'aide de cet algorithme, il faut avoir la clée puis effectuer l'opération inverse: 
&rarr; remplacer ``out[i] = (out[i - 1] + in[i] + k) % 256;`` par ``out[i] = (in[i] - in[i - 1] - k) % 256;``.
$$
psnr = 8.73
$$

$$
entropy = 7.99
$$

| Histogramme image de base | Histogramme image chiffrée |
| :-----------------------: | :------------------------: |
|      ![](histo.png)       |     ![](histo_sub.png)     |

Avec le chiffrement par *substitution* les valeurs des pixels sont modifiées par rapport à l'image de base, ainsi l'histogramme est différent.

## Approche *brute force* 
On remarque qu'une image chiffrée par substitution a une entropie proche de 8 (*7.99*). Puisque lorsque l'on tente de déchiffrer une image chiffrée par substitution sans avoir la bonne clée on obtient à nouveau une image chiffrée on peut alors opter pour une approche **brute force** pour trouver la clée utilisée pour chiffrer l'image par substitution (tant que celle-ci n'est pas trop grande).

```c++
int current_key = 0;
do{
        allocation_tableau(ImgOut, OCTET, nTaille);
        substitute(ImgIn, ImgOut, nTaille, current_key, -1); //tentative de décryptage avec la clée courante

        remplir_histo(ImgOut, nTaille, histo, 256);
        remplir_freq(nTaille, histo, 256, freq);

        entropy = computeEntropy(freq, 256);
        current_key ++;
    }while(entropy >= 7.99);

current_key -= 1;
```



Ainsi j'ai pu décrypter une image que l'on m'a envoyé en forçant la clée (c'était 0).					

| Image chiffrée par substitution |   Image décryptée    |
| :-----------------------------: | :------------------: |
|   ![](otter_substituted.png)    | ![](unsub_otter.png) |

​					

