#include <stdio.h>
#include <string>
#include <filesystem>
#include <iostream>
#include "image_ppm.h"

namespace fs = std::filesystem;

int main(int argc, char** argv) {
    int nH, nW, nTaille;
    
    if (argc != 1) {
        printf("Usage: No arguments\n"); 
        exit (1) ;
    }
    
    OCTET *ImgIn, *ImgOut;
    nH = 247;
    nW = 247;
    nTaille = nH * nW;
    allocation_tableau(ImgIn, OCTET, nTaille);
    allocation_tableau(ImgOut, OCTET, nTaille);

    //init
    int* sumImg = new int[nTaille];
    for(int i = 0; i < nTaille; i ++)
        sumImg[i] = 0;


    //Avg image of cockroach
    int dirSize = 0;
    std::string path = "img/cockroach/flattened/";
    for (const auto & entry : fs::directory_iterator(path)) {
        // std::cout << entry.path() << std::endl;
        lire_image_pgm(&entry.path().string()[0], ImgIn, nTaille);
        
        for(int i = 0; i < nTaille; i ++)
            sumImg[i] += ImgIn[i];


        dirSize ++;
    }

    for(int i = 0; i < nTaille; i ++) 
        ImgOut[i] = sumImg[i] / dirSize;

    ecrire_image_pgm("avgImg_cockroach.pgm", ImgOut, nH, nW);

    for(int i = 0; i < nTaille; i ++)
        sumImg[i] = 0;

    //Avg image of raccoon
    dirSize = 0;
    path = "img/raccoon/flattened/";
    for (const auto & entry : fs::directory_iterator(path)) {
        // std::cout << entry.path() << std::endl;
        lire_image_pgm(&entry.path().string()[0], ImgIn, nTaille);
        
        for(int i = 0; i < nTaille; i ++)
            sumImg[i] += ImgIn[i];


        dirSize ++;
    }

    for(int i = 0; i < nTaille; i ++) 
        ImgOut[i] = sumImg[i] / dirSize;

    ecrire_image_pgm("avgImg_raccoon.pgm", ImgOut, nH, nW);


    free(ImgIn);
    free(ImgOut);
    delete[] sumImg;
    return 1;
}