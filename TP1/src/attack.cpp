#include <stdio.h>
#include "image_ppm.h"


float computeEntropy(float* p, int p_size) {
    float res = 0;
    for(int i = 0; i < p_size; i ++){
        if(p[i] > 0.001)
            res += p[i] * log2(p[i]);
    }
    return -res;
}


//SUBSTITUTE
//opposite: 1 -> encrypt, -1 -> decrypt
void substitute(OCTET* in, OCTET* out, int size, int key, int op = 1) {
    srand(key);
    int k = rand() % 256;
    out[0] = k + in[0];

    for(int i = 1; i < size; i ++) {
        k = rand() % 256;
        if(op < 0){
            out[i] = (in[i] + op*in[i - 1] + op*k) % 256;

        }else{

            out[i] = (out[i - 1] + op*in[i] + op*k) % 256;
        }
    }
}


int main(int argc, char** argv) {
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    int histo[256];
    float freq[256];


    if (argc != 2) 
        {
        printf("Usage: ImageIn.pgm imageOut.pgm\n"); 
        exit (1) ;
        }
    
    sscanf (argv[1],"%s",cNomImgLue) ;

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    

    int current_key = 0;
    float entropy;

    do{
        allocation_tableau(ImgOut, OCTET, nTaille);
        substitute(ImgIn, ImgOut, nTaille, current_key, -1);

        remplir_histo(ImgOut, nTaille, histo, 256);
        remplir_freq(nTaille, histo, 256, freq);

        entropy = computeEntropy(freq, 256);
        current_key ++;
        printf("entropy: %f\n", entropy);
    }while(entropy >= 7.99);

    current_key -= 1;
    printf("KEY IS %d\n", current_key);


    free(ImgIn);
    return 1;
}