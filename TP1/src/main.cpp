#include <stdio.h>
#include "image_ppm.h"

// SCRAMBLE

int nextAvailable(int* map, int size, int index) {
    int i = index;

    while(map[i] == 1) {
        if(i == size - 1){
            i = 0;
        }else{
            i ++;
        }
    }
    
    return i;
}

void scramble(OCTET* in, OCTET* out, int size, int* map, int key) {
    int pos;
    // rand seed is key
    srand(key);
    
    for(int i = 0; i < size; i ++) {
        pos = rand() % size;

        if(map[pos] != -1)
            pos = nextAvailable(map, size, pos);

        out[pos] = in[i];
        map[pos] = 1;
    }
}

void unscramble(OCTET* in, OCTET* out, int size, int* map, int key) {
    int pos;
    // rand seed is key
    srand(key);
    
    for(int i = 0; i < size; i ++) {
        pos = rand() % size;

        if(map[pos] != -1)
            pos = nextAvailable(map, size, pos);

        out[i] = in[pos];
        map[pos] = 1;
    }
}

float computeEntropy(float* p, int p_size) {
    float res = 0;
    for(int i = 0; i < p_size; i ++){
        if(p[i] > 0.001)
            res += p[i] * log2(p[i]);
    }
    return -res;
}


//SUBSTITUTE
//opposite: 1 -> encrypt, -1 -> decrypt
void substitute(OCTET* in, OCTET* out, int size, int key, int op = 1) {
    srand(key);
    int k = rand() % 256;
    out[0] = k + in[0];

    for(int i = 1; i < size; i ++) {
        k = rand() % 256;
        if(op < 0){
            out[i] = (in[i] + op*in[i - 1] + op*k) % 256;

        }else{

            out[i] = (out[i - 1] + op*in[i] + op*k) % 256;
        }
    }
}


int main(int argc, char** argv) {
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    int histo[256];
    float freq[256];


    if (argc != 3) 
        {
        printf("Usage: ImageIn.pgm imageOut.pgm\n"); 
        exit (1) ;
        }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    allocation_tableau(ImgOut, OCTET, nTaille);
    
    int map[nTaille];
    for(int i = 0; i < nTaille; i ++)
        map[i] = -1;

    // scramble(ImgIn, ImgOut, nTaille, map, 12);
    // unscramble(ImgIn, ImgOut, nTaille, map, 12);
    // substitute(ImgIn, ImgOut, nTaille, 46);
    // substitute(ImgIn, ImgOut, nTaille, 0, -1);

    int current_key = 0;
    remplir_histo(ImgIn, nTaille, histo, 256);

    for(int i = 0; i < 256; i ++) {
        printf("%d %d\n", i, histo[i]);
    }
    // remplir_freq(nTaille, histo, 256, freq);

    // float entropy = computeEntropy(freq, 256);

    // ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn);
    return 1;
}