

# TP3 Insertion de données cachées par substitution
>*seed = 4*
>**PSNR, histogramme**

| Image hôte           | Image cachée            |
| -------------------- | ----------------------- |
| ![[img/capy.png]] | ![](img/14_Medical.png) |


## Approche naïve
Pour cette première approche, les pixels de l'imagette vont être cachés dans l'image hôte les uns à la suite des autres. Ainsi **le premier pixel de l'imagette** va être injecté dans **les 8 premiers pixels de l'image hôte** à la place du lsb.
![[Drawing 2022-09-27 10.34.36.excalidraw]]

| Image hôte           | Image modifiée           |
| -------------------- | ----------------------- |
| ![[hidden_med_capy_naive.png]] | ![](img/hidden_med_capy_naive.png) |

- $psnr = 1$
- $entropy = 1$

## Approche Prng


## Extraction d'image cachée
1. **Naïve**
2. **Prng**

&rarr; Modifier le LSB: meilleur solution