#!/usr/bin/env python

import tensorflow as tf
from tensorflow import keras

import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

import sys,os
from importlib import reload
sys.path.append('..')


# ConfusionMatrix plotting class
class estimator:
    _estimation_type = ''
    classes = []

    def __init__(self, model, classes):
        self.model = model
        self._estimation_type = 'classifier'
        self.classes = classes

    def predict(self, X):
        y_prob = self.model.predict(X)
        y_pred = y_prob.argmax(axis=1)
        return y_pred

class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
####################################################################


(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

x_train = x_train.reshape(-1,28,28,1)
x_test  = x_test.reshape(-1,28,28,1)

print("x_train : ",x_train.shape)
print("y_train : ",y_train.shape)
print("x_test  : ",x_test.shape)
print("y_test  : ",y_test.shape)

#pour la suite, à vous de jouer, bon courage :)

print("Avant normalisation: Min={}, Max={}".format(x_train.min(), x_train.max()))

xmax = x_train.max()
x_train = x_train / xmax
x_test = x_test / xmax

print("Après normalisation : Min={} Max={}".format(x_train.min(), x_train.max()))


# Layers
model = keras.models.Sequential()
model.add(keras.layers.Input((28, 28 ,1)))

model.add(keras.layers.Conv2D(2, (3,3), activation='relu'))
model.add(keras.layers.MaxPooling2D((2,2)))

model.add(keras.layers.Conv2D(4, (3,3), activation='relu'))
model.add(keras.layers.MaxPooling2D((2,2)))

model.add(keras.layers.Conv2D(16, (3,3), activation='relu'))
model.add(keras.layers.MaxPooling2D((2,2)))

# Fully connected
model.add(keras.layers.Flatten())
model.add(keras.layers.Dense(100, activation='relu'))

model.add(keras.layers.Dense(10, activation='softmax'))


# Evaluation
model.summary()
model.compile(
    optimizer='adam',
    loss='sparse_categorical_crossentropy',
    metrics=['accuracy']
)

batch_size = 700
epochs = 32

history = model.fit(
    x_train, y_train,
    batch_size=batch_size,
    epochs = epochs,
    verbose = 1,
    validation_data=(x_test, y_test)
)

score = model.evaluate(x_test, y_test, verbose=0)
print(f'Test loss : {score[0]:4.4f}')
print(f'Test accuracy : {score[1]:4.4f}')

model.metrics_names
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()


classifier = estimator(model, class_names)

y_pred = classifier.predict(x_test)
print(y_pred)

cf_matrix = confusion_matrix(y_test, y_pred)
print(cf_matrix)

fig = plt.figure()
plt.matshow(cf_matrix)
plt.title('Confusion Matrix')
plt.colorbar()
plt.ylabel('True Label')
plt.xlabel('Predicated Label')


for (i, j), z in np.ndenumerate(cf_matrix):
    plt.text(j, i, '{}'.format(z), ha='center', va='center')

plt.savefig('confusion_matrix.jpg')
