#include <stdio.h>
#include "image_ppm.h"

int nextAvailable(int* map, int size, int index) {
    int i = index;

    while(map[i] == 1) {
        if(i == size - 1){
            i = 0;
        }else{
            i ++;
        }
    }
    
    return i;
}

void rnaiveStegano(OCTET* in, OCTET* out, int inSize, int outSize) {
    int word;
    int shift, lsb;
    for(int i = 0; i < outSize; i ++) {
        word = 0;
        shift = 0;
        for(int j = 0; j < 8; j ++) {
            /*
                bits are at in[i * 8 + j]. (concat 8bits to reconstruct char in out[i])
                w += (in[i*8+j] & 1) << (7-j);
            */
            lsb = (in[i*8+j] & 1);

            word += lsb * (int) pow(2, shift);
            shift ++;

        }

        out[i] = (char) word;

    }
}

void rprngStegano(OCTET* in, OCTET* out, int inSize, int outSize, int seed, int nthBit) {
    srand(seed);
    int randIndex;
    int map[inSize];
    int word, shift, lsb;
    // int shift = pow(2, nthBit);
    // int k = 0;

    for(int i = 0; i < inSize; i ++) {
        map[i] = -1;
    }

    for(int i = 0; i < outSize; i ++) {
        word = 0;
        shift = 0;
        for(int j = 0; j < 8; j ++) {

            randIndex = rand() % inSize;
            // printf("out\n");

            if(map[randIndex] == 1) {
                randIndex = nextAvailable(map, inSize, randIndex);
            }

            /*
                RandIndex -> indice du pixel contenant un bit de l'imagette 
            */
            lsb = in[randIndex] & 1 << nthBit; 
            word += lsb * 1 << shift;

            map[randIndex] = 1;
            shift ++;
        }

        out[i] = (char) word;
    }
}

int main(int argc, char** argv) {
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTailleHost, nTailleHidden;
    
    if (argc != 3) {
        printf("Usage: ImageIn.pgm imageOut.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue);
    sscanf (argv[2],"%s",cNomImgEcrite);

    OCTET *ImgIn1, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTailleHost = nH * nW;
    allocation_tableau(ImgIn1, OCTET, nTailleHost);
    lire_image_pgm(cNomImgLue, ImgIn1, nH * nW);


    nTailleHidden = 64 * 64;
    allocation_tableau(ImgOut, OCTET, nTailleHidden);
    
    // rnaiveStegano(ImgIn1, ImgOut, nTailleHost, nTailleHidden);
    rprngStegano(ImgIn1, ImgOut, nTailleHost, nTailleHidden, 4, 5);
    ecrire_image_pgm(cNomImgEcrite, ImgOut, 64, 64);

    free(ImgIn1);
    free(ImgOut);
    return 1;
}