#include <stdio.h>
#include <string>
#include "image_ppm.h"

void applyFilter(OCTET* in, OCTET* out, int width, float* filter) {
    int index, value;
    for(int i = 1; i < width-1; i ++) {
        for(int j = 1; j < width-1; j ++) {
            index = i * width + j;
            value = 0;
            
            for(int k = 0; k < 3; k ++) {
                for(int l = 0; l < 3; l ++) {
                    value += filter[k * 3 + l] * in[index + width*(k-1) + l-1];
                }
            }

            // printf("%d\n", value);

            out[index] = value;
        }
    }
}

void applyReLU(OCTET* img, int size) {
    for(int i = 0; i < size; i ++)
        img[i] = img[i] > 0 ? img[i] : 0;
}

OCTET* downSample(OCTET* img, int width) {
    OCTET* outImg;
    allocation_tableau(outImg, OCTET, (width*width)/2);
    int outIndex = 0;
    int value, index;

    for(int i = 0; i < width-1; i +=2) {
        for(int j = 0; j < width-1; j +=2) {
            index = i * width + j;
            //Max value of block
            value = std::fmax( 
                std::fmax(img[index], img[index + 1]),
                std::fmax(img[index + width], img[index + width + 1])
            );
            outImg[outIndex] = value;
            outIndex ++;
        }
    }

    return outImg;
}

OCTET* computeLayers(OCTET* inputImg, int nW) {

    float LPFilter[9] = {
        1./9, 1./9, 1./9,
        1./9, 1./9, 1./9,
        1./9, 1./9, 1./9
    };

    float LPFilter2[9] = {
        1./10, 1./10, 1./10,
        1./10, 2./10, 1./10,
        1./10, 1./10, 1./10
    };

    float HPFilter[9] = {
        0, -1, 0,
        -1, 5, -1,
        0, -1, 0
    };

    float HPFilter2[9] = {
        -1, -1, -1,
        -1, 9, -1,
        -1, -1, -1
    };

    float HPFilter3[9] = {
        0, -1, 0,
        -1, 4, -1,
        0, -1, 0
    };

    float *filtersLayer1[9] = {
        LPFilter, LPFilter2, HPFilter, HPFilter2, HPFilter3
    };

    float *filtersLayer2[9] = {
        LPFilter, LPFilter2, HPFilter,
    };

    OCTET *outputImg, *downSampledImg, *outputImg2, *downSampledImg2;
    allocation_tableau(outputImg, OCTET, nW*nW);
    allocation_tableau(outputImg2, OCTET, nW/2*nW/2);

    //flattened array
    OCTET* flattened;
    int flattenedSize = nW/4 * nW/4 * 15;
    allocation_tableau(flattened, OCTET, flattenedSize);
    
    int flattened_index = 0;
    for(int i = 0; i < 5; i ++) {
        //first layer, apply 5 filters
        applyFilter(inputImg, outputImg, nW, filtersLayer1[i]);
        applyReLU(outputImg, nW*nW);
        downSampledImg = downSample(outputImg, nW);

        for(int j = 0; j < 3; j ++) {
            //second layer, for every downsampled image, create 3 new images with filters applied + downsampled
            applyFilter(downSampledImg, outputImg2, nW/2, filtersLayer2[j]);
            applyReLU(outputImg2, nW/2*nW/2);

            downSampledImg2 = downSample(outputImg2, nW/2);

            for(int k = 0; k < nW/4 * nW/4; k ++) {
                flattened[flattened_index] = downSampledImg2[k];
                flattened_index++;
            }
        }

    }

    free(outputImg);
    free(outputImg2);
    free(downSampledImg);
    free(downSampledImg2);

    return flattened;
}

int main(int argc, char** argv) {
    char cNomImgLue[250];
    int nH, nW, nTaille;
    
    if (argc != 2) {
        printf("Usage: ImageIn.pgm imageOut.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue);

    OCTET *ImgIn, *ImgAvg1, *ImgAvg2, *ImgInFlat;
    int flatImgSize = 247 * 247;

    // Average images setup
    allocation_tableau(ImgAvg1, OCTET, flatImgSize);
    allocation_tableau(ImgAvg2, OCTET, flatImgSize);
    allocation_tableau(ImgInFlat, OCTET, flatImgSize);
    lire_image_pgm("img/avgImg_cockroach.pgm", ImgAvg1, flatImgSize);
    lire_image_pgm("img/avgImg_raccoon.pgm", ImgAvg2, flatImgSize);
 
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    ImgInFlat = computeLayers(ImgIn, nW);
    float PSNR1 = psnr(ImgAvg1, ImgInFlat, flatImgSize);
    float PSNR2 = psnr(ImgAvg2, ImgInFlat, flatImgSize);

    if(PSNR1 > PSNR2) {
        printf("There is a cockroach in this image.(%fc, %fr)\n", PSNR1, PSNR2);
    } else {
        printf("There is a raccoon in this image. (%fc, %fr)\n", PSNR1, PSNR2);
    }


    free(ImgIn);
    free(ImgInFlat);
    return 1;
}