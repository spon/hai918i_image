#include <stdio.h>
#include "image_ppm.h"
/*
    Xor entre imgIn1 et imgIn2 dans imgOut (les 2 images doivent avoir la même taille)
*/

void XorImg(OCTET* in1, OCTET* in2, OCTET* out, int size) {
    for(int i = 0; i < size; i ++) {
        out[i] = in1[i] ^ in2[i];
    }
}

int nextAvailable(int* map, int size, int index) {
    int i = index;

    while(map[i] == 1) {
        if(i == size - 1){
            i = 0;
        }else{
            i ++;
        }
    }
    
    return i;
}

void naiveStegano(OCTET* host, OCTET* hidden, OCTET* out, int hostSize, int hiddenSize) {
    // int hiddenBits = hiddenSize * 8;
    int j = 0;
    int k = 0;
    for(int i = 0; i < hostSize; i ++) {
        if(j < hiddenSize) {

            out[i] = host[i] - host[i]%2 + ((hidden[j] >> k) & 1);
            k ++;

            if(k == 8) {
                k = 0;
                j ++;
            }
        }
        else
        {
            out[i] = host[i];
        }
    }
}

void prngStegano(OCTET* host, OCTET* hidden, OCTET* out, int hostSize, int hiddenSize, int seed, int nthBit) {
    srand(seed);
    int randIndex;
    int map[hostSize];
    // int shift = pow(2, nthBit);
    // int k = 0;

    for(int i = 0; i < hostSize; i ++) {
        map[i] = -1;
        out[i] = host[i];
    }

    for(int i = 0; i < hiddenSize; i ++) {
        for(int j = 0; j < 8; j ++) {
            randIndex = rand() % hostSize;

            if(map[randIndex] == 1) {
                randIndex = nextAvailable(map, hostSize, randIndex);
            }

            out[randIndex] ^= (-((hidden[i] >> j) & 1) ^ out[randIndex]) & (1UL << nthBit);
            map[randIndex] = 1;
        }
    }

}

int main(int argc, char** argv) {
    char cNomImgLue1[250],cNomImgLue2[250], cNomImgEcrite[250];
    int nH, nW, nTailleHost, nTailleHidden;
    
    if (argc != 4) {
        printf("Usage: ImageHost.pgm ImageHidden.pgm imageOut.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue1);
    sscanf (argv[2],"%s",cNomImgLue2);
    sscanf (argv[3],"%s",cNomImgEcrite);

    OCTET *ImgIn1, *ImgIn2, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
    nTailleHost = nH * nW;
    allocation_tableau(ImgIn1, OCTET, nTailleHost);
    lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);


    lire_nb_lignes_colonnes_image_pgm(cNomImgLue2, &nH, &nW);
    nTailleHidden = nH * nW;
    allocation_tableau(ImgIn2, OCTET, nTailleHidden);
    lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
    
    allocation_tableau(ImgOut, OCTET, nTailleHost);

    
    // naiveStegano(ImgIn1, ImgIn2, ImgOut, nTailleHost, nTailleHidden);
    prngStegano(ImgIn1, ImgIn2, ImgOut, nTailleHost, nTailleHidden, 4, 5);
    // XorImg(ImgIn1, ImgIn2, ImgOut, nTaille);

    ecrire_image_pgm(cNomImgEcrite, ImgOut, 1024, 1024);

    free(ImgIn1);
    free(ImgIn2);
    free(ImgOut);
    return 1;
}