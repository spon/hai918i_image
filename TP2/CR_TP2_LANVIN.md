

# TP2 Chiffrement d'images

![](img/capy.png)

## Chiffrement d'image par AES

*Chiffrement par blocs ECB*

|                      Image de base                      |              Image chiffrée par AES avec *ECB*               |
| :-----------------------------------------------------: | :----------------------------------------------------------: |
|    ![](/home/spon/M2/hai918i_image/TP2/img/capy.png)    |  ![](/home/spon/M2/hai918i_image/TP2/img/capy_enc_ecb.png)   |
| ![](/home/spon/M2/hai918i_image/TP2/img/histo_capy.png) | ![](/home/spon/M2/hai918i_image/TP2/img/histo_capy_enc_ecb.png) |

Analyse statistique de l'image chiffrée:

- $psnr=8.74$

- $entropy = 7.99$

  

## Modes de chiffrement AES

Cette fois-ci nous utilisons une image médicale.

1. **ECB**

   |                      Image de base                      |                Image chiffrée (AES avec ECB)                 |
   | :-----------------------------------------------------: | :----------------------------------------------------------: |
   | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical.png) | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical_enc_ecb.png) |
   | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med.png)  | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med_enc_ecb.png) |


   On remarque des patterns sur l'image chiffrée lorsque l'on utilise la méthode **ECB** pour chiffrer l'image, on peut alors distinguer des objets contenus dans l'image de base à partir de l'image chiffrée.

   Analyse statistique de l'image chiffrée:

   - $psnr=5.30$
   - $entropy = 7.72$

2. **CBC**

   |                      Image de base                      |                 Image chiffrée(AES avec CBC)                 |
   | :-----------------------------------------------------: | :----------------------------------------------------------: |
   | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical.png) | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical_enc_cbc.png) |
   | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med.png)  | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med_enc_cbc.png) |

   Cette fois-ci **aucun objet n'est distinguable** dans l'image chiffrée.

   Analyse statistique de l'image chiffrée:

   - $psnr=5.48$
   - $entropy = 7.99$

     

     
     

3. **CFB**

   |                      Image de base                      |                 Image chiffrée(AES avec CFB)                 |
   | :-----------------------------------------------------: | :----------------------------------------------------------: |
   | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical.png) | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical_enc_cfb.png) |
   | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med.png)  | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med_enc_cfb.png) |

   Analyse statistique de l'image chiffrée:

   - $psnr=5.48$
   - $entropy = 7.99$

4. **OFB**

   |                      Image de base                      |                 Image chiffrée(AES avec OFB)                 |
   | :-----------------------------------------------------: | :----------------------------------------------------------: |
   | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical.png) | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical_enc_ofb.png) |
   | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med.png)  | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med_enc_ofb.png) |

   Analyse statistique de l'image chiffrée:

   - $psnr=5.80$
   - $entropy = 7.99$

5. **CTR**

   |                      Image de base                      |                 Image chiffrée(AES avec CTR)                 |
   | :-----------------------------------------------------: | :----------------------------------------------------------: |
   | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical.png) | ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical_enc_ctr.png) |
   | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med.png)  | ![](/home/spon/M2/hai918i_image/TP2/img/histo_med_enc_ctr.png) |

   On distingue avec ce mode des patterns correspondant a certains objets contenus dans l'image en clair.

   Analyse statistique de l'image chiffrée:

   - $psnr=5.47$
   - $entropy = 7.99$
   
     
   
     

## Correction d'image chiffrée bruitée

*Le but de cette partie est de savoir s'il est possible de retrouver une image en la déchiffrant alors qu'elle aurait été bruitée après chiffrement.* 

1. Ajout de bruit pseudo-aléatoire en modifiant **un bit** aléatoire sur un bloc. Chaque bloc a 20% de chances d'être bruité.

   ```c++
   void corrupt(OCTET* img, int size, int seed, int block_size) {
       srand(seed);
       int prob, bit_index, pixel_index, rand_index;
   
       for(int i = 0; i < size; i += block_size) {
           prob = rand() % 5;
           if(prob == 1) {
               //corrompre un bit sur le bloc en cours
               rand_index = rand() % 128;
               pixel_index = rand_index / 8;
               bit_index = rand_index % 8;
   
               //pixel à corrompre
               img[i + pixel_index] ^=  (1 << bit_index);
   
           }
       }
   }
   ```

2. Afin de comparer efficacement les image bruitées déchiffrées on utilise la même graîne pour **générer le même bruit sur chacunes des images chiffrées**.

3. Résultats obtenus:

   |                            ECB                            |                            CBC                            |
   | :-------------------------------------------------------: | :-------------------------------------------------------: |
   | ![](/home/spon/M2/hai918i_image/TP2/img/med_ecb_corr.png) | ![](/home/spon/M2/hai918i_image/TP2/img/med_cbc_corr.png) |

   

   

   

   

   |                          **CFB**                          |                          **OFB**                          |
   | :-------------------------------------------------------: | :-------------------------------------------------------: |
   | ![](/home/spon/M2/hai918i_image/TP2/img/med_cfb_corr.png) | ![](/home/spon/M2/hai918i_image/TP2/img/med_ofb_corr.png) |
   |                          **CTR**                          |                     **Image de base**                     |
   | ![](/home/spon/M2/hai918i_image/TP2/img/med_ctr_corr.png) |  ![](/home/spon/M2/hai918i_image/TP2/img/14_Medical.png)  |

   

On remarque alors qu'avec les modes **CTR** et **OFB**, l'image déchiffrée est exactement l'image de base à laquelle on a ajouté le bruit. En utilisant les autres modes on voit que lorsqu'un pixel est modifié alors tout le bloc le contenant est erroné, le bruit se **propage dans le bloc**.

Les modes **CTR** et **OFB** sont les plus intéressants à utiliser pour obtenir une image assez fidèle à l'image de base lorsque l'image chiffrée est bruitée.