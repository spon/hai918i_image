#include <stdio.h>
#include "image_ppm.h"

void corrupt(OCTET* img, int size, int seed, int block_size) {
    srand(seed);
    int prob, bit_index, pixel_index, rand_index;

    for(int i = 0; i < size; i += block_size) {
        prob = rand() % 5;
        if(prob == 1) {
            //corrompre un bit sur le bloc en cours
            rand_index = rand() % 128;
            pixel_index = rand_index / 8;
            bit_index = rand_index % 8;

            //pixel à corrompre
            img[i + pixel_index] ^=  (1 << bit_index);

        }
    }
}

int main(int argc, char** argv) {
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    

    if (argc != 3) {
        printf("Usage: ImageIn.pgm imageOut.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue);
    sscanf (argv[2],"%s",cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    allocation_tableau(ImgOut, OCTET, nTaille);

    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    copy_pgm(ImgIn, ImgOut, nTaille);

    corrupt(ImgOut, nTaille, 1, 16);
    
    ecrire_image_pgm(cNomImgEcrite, ImgOut, nW, nH);
    free(ImgIn);
    free(ImgOut);
    return 1;
}