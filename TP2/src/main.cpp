#include <stdio.h>
#include "image_ppm.h"
/*
    Analyse du PSNR(entre les 2) + entropie des 2 images.
*/
int main(int argc, char** argv) {
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    // int histo[256];

    if (argc != 3) {
        printf("Usage: ImageIn.pgm imageOut.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue);
    sscanf (argv[2],"%s",cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    allocation_tableau(ImgOut, OCTET, nTaille);

    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    lire_image_pgm(cNomImgEcrite, ImgOut, nH * nW);

    printf("PSNR: %f\n", psnr(ImgIn, ImgOut, nTaille));
    printf("entropy first: %f\n", computeEntropy(ImgIn, nTaille));
    printf("entropy second: %f\n", computeEntropy(ImgOut, nTaille));

    free(ImgIn);
    free(ImgOut);
    return 1;
}